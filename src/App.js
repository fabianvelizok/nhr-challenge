import React, { useState, useEffect } from 'react';
import TenantList from './components/TenantList/TenantList';
import { Service } from './Service';

// IMPROVEMENT: Move constants to a proper file
const TAB_OPTIONS = {
  ALL: 'all',
  PAYMENT_LATE: 'paymentLate',
  LEASE_ENDS_LESS_THAN_A_MONTH: 'leaseEndsInLessThanAMonth'
};

// IMPROVEMENT: Move constants to a proper file
const PAYMENT_STATUS = {
  CURRENT: 'CURRENT',
  LATE: 'LATE'
};

// IMPROVEMENT: Add some kind of loader to indicate the user the app is
// fetching of doing something.

function App() {
  const [tenants, setTenants] = useState([]);
  const [currentTab, setCurrentTab] = useState(TAB_OPTIONS.ALL);
  const [error, setError] = useState(false);

  useEffect(() => {
    // IMPROVEMENT: Create custom hook with this logic. DRY.
    async function fetchTenants() {
      try {
        const response = await Service.getTenants();
        setTenants(response);
      } catch (e) {
        console.log("Error fetching tenants.");
        return setError(true);
      }
    }

    fetchTenants();
  }, []);

  useEffect(() => {
    // NOTE: I am going to call fetchTenants again and filter results although I could 
    // filter in memory results.
    async function fetchTenants() {
      let originalResponse;

      try {
        originalResponse = await Service.getTenants();
      } catch(e) {
        console.log("Error fetching tenants.");
        // IMPROVEMENT: Improve the error shownt to user
        return setError(true);
      }
      
      // IMPROVEMENT: Change for a switch for readability
      if (currentTab === TAB_OPTIONS.ALL) {
        setTenants(originalResponse);
      } else if (currentTab === TAB_OPTIONS.PAYMENT_LATE) {
        const filteredList = originalResponse.filter((tenant) => {
          return tenant.paymentStatus === PAYMENT_STATUS.LATE
        });

        setTenants(filteredList);
      } else {
        const filteredList = originalResponse.filter((tenant) => {
          const tenantLeaseEndDate = new Date(tenant.leaseEndDate);
          
          const currentDate = new Date();
          const limitDate = new Date(currentDate.setMonth(currentDate.getMonth() + 1));

          if (tenantLeaseEndDate.getTime() < limitDate.getTime()) {
            return tenant;
          }
        });

        setTenants(filteredList);
      }
    }

    fetchTenants();
  }, [currentTab])


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = new FormData(event.target);

    const name = data.get('name');
    const paymentStatus = data.get('paymentStatus');
    const leaseEndDate = data.get('leaseEndDate');

    // Auxiliar vars
    const currentDate = new Date();
    const parsedLeaseEndDate = new Date(leaseEndDate);

    // Validate fields
    if (
      !name || name.length > 25 // Name validation
      || !leaseEndDate || currentDate > parsedLeaseEndDate // Lease end data validation
    ) {
      // IMPROVEMENT: We should disable the save button and show the user a
      // message with the error.
      return console.log("Check your inputs.");
    }

    const newTenant = {
      name,
      paymentStatus,
      leaseEndDate,
    };

    try {
      const tenant = await Service.addTenant(newTenant);
      const currentTenants = [...tenants, tenant];
      // We also could call the API to get updated list
      setTenants(currentTenants);
    } catch (e) {
      console.log("Error adding a tenant.");
      return setError(true);
    }
  }

  const handleDeleteTenant = async (id) => {
    try {
      const response = await Service.deleteTenant(id);
      
      if (response === "OK") {
        const filteredList = tenants.filter(tenant => {
          return tenant.id !== id
        });

        setTenants(filteredList);
      }
    } catch (e) {
      console.log("Error deleting a tenant.");
      return setError(true);
    }
  }

  // TODO: Complete sort methods
  const handleSortTenants = () => {

  }

  // IMPROVEMENT: Improve currentTab className logic. Separate in a function.
  return (
      <div>
        {/* IMPROVEMENT: Improve the error error UI */}
        {error ? (
          <div>An error has occurred. Try reloading the app.</div>
        ) : (
          <div>
            <div className="container">
              <h1>Tenants</h1>
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <button
                    className={`${currentTab === TAB_OPTIONS.ALL ? "nav-link active" : "nav-link"}`}
                    onClick={() => setCurrentTab(TAB_OPTIONS.ALL)}
                  >
                    All
                  </button>
                </li>
                <li className="nav-item">
                  <button
                    className={`${currentTab === TAB_OPTIONS.PAYMENT_LATE ? "nav-link active" : "nav-link"}`}
                    onClick={() => setCurrentTab(TAB_OPTIONS.PAYMENT_LATE)}
                  >
                    Payment is late
                  </button>
                </li>
                <li className="nav-item">
                  <button
                    className={`${currentTab === TAB_OPTIONS.LEASE_ENDS_LESS_THAN_A_MONTH ? "nav-link active" : "nav-link"}`}
                    onClick={() => setCurrentTab(TAB_OPTIONS.LEASE_ENDS_LESS_THAN_A_MONTH)}
                  >
                    Lease ends in less than a month
                  </button>
                </li>
              </ul>

            {/* IMPROVEMENT: The idea was to separate logic and components inside its own folder, but not enough time. */}
              <TenantList tenants={tenants} handleDeleteTenant={handleDeleteTenant} handleSortTenants={handleSortTenants} />

            </div>
            <div className="container">
              <button className="btn btn-secondary">Add Tenant</button>
            </div>
            <div className="container">
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label>Name</label>
                  <input className="form-control" name="name" required maxlength="25"/>
                </div>
                <div className="form-group">
                  <label>Payment Status</label>
                  <select className="form-control" name="paymentStatus" required>
                    <option>CURRENT</option>
                    <option>LATE</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Lease End Date</label>
                  <input className="form-control" name="leaseEndDate" required placeholder="YYYY-MM-DD"/>
                </div>
                <button className="btn btn-primary">Save</button>
                <button className="btn">Cancel</button>
              </form>
            </div>
          </div>
        )}
    </div>
  );
}

export default App;
