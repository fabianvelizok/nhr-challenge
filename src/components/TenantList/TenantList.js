import React from 'react';

const TenantList = (props) => {
  const { tenants, handleDeleteTenant, handleSortTenants } = props;

  // TODO: Implement sort tenants
  
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Payment Status</th>
            <th>Lease End Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {tenants.length ? 
            tenants.map((tenant) => {
              return (
                <tr>
                  <th>{tenant.id}</th>
                  <td>{tenant.name}</td>
                  <td>{tenant.paymentStatus}</td>
                  <td>{tenant.leaseEndDate}</td>
                  <td>
                    <button className="btn btn-danger" onClick={() => handleDeleteTenant(tenant.id)}>Delete</button>
                  </td>
                </tr>
              )
            })
          : (
            <tr>
              <td>No tenants.</td>
            </tr>
          )
        }
        </tbody>
      </table>
    </div>
  );
}

export default TenantList;